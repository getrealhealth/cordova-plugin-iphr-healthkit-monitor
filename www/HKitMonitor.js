var exec = require('cordova/exec');

let HKitMonitor = {};

HKitMonitor.monitorSampleType = function (success, error, sampleType, unit, recordId, personId, notificationUrl) {
    exec(success, error, 'HKitMonitor', 'monitorSampleType', [{'sampleType':sampleType, 'unit' : unit, 'recordId' : recordId, 'personId' : personId, 'notificationUrl' : notificationUrl}]);
};

module.exports = HKitMonitor;