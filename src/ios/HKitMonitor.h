#import <Cordova/CDVPlugin.h>
#import "AppDelegate.h"
#import <HealthKit/HealthKit.h>

@interface HKitMonitor : CDVPlugin
-(void)pluginInitialize;
-(void)monitorSampleType:(NSString *)sampleTypeString unitString:(NSString *)unitString recordId:(NSString *) recordId personId:(NSString *) personId notificationUrl: (NSString *)notificationUrl;
@end 
