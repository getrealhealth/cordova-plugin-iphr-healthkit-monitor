#import "HKitMonitor.h"
#import "SAMKeychain.h"

@interface HKitMonitor (internalHelper)
+ (NSString *)stringFromDate:(NSDate *)date;
+ (HKSampleType *)getHKSampleType:(NSString *)elem;
-(NSString*)jsonFromDictionary:(NSDictionary*)data;
-(void)sendNotification:(NSString*)notificationUrl data:(NSMutableDictionary*)data completionHandler:(void(^)(BOOL success))completionHandler;
-(void)querySampleType:(HKSampleType*)type sampleTypeString:(NSString*)sampleTypeString startDate:(NSDate*)startDate endDate:(NSDate*)endDate unit:(HKUnit*)unit limit:(NSUInteger)limit ascending:(BOOL)ascending completionHandler:(void(^)(NSMutableArray *results))completionHandler;
-(NSMutableDictionary*)transformSample:(HKSample*)sample sampleTypeString:(NSString*) sampleTypeString unit:(HKUnit*)unit;
-(void)finishedLaunching:(NSNotification*)notification;
+ (HKHealthStore *)sharedHealthStore;
@end

@implementation HKitMonitor (internalHelper)

static NSString *const HKPluginKeyStartDate = @"startDate";
static NSString *const HKPluginKeyEndDate = @"endDate";
static NSString *const HKPluginKeySourceName = @"source";
static NSString *const HKPluginKeySourceBundleId = @"sourceBundleId";
static NSString *const HKPluginKeyUUID = @"UUID";
static NSString *const HKPluginKeyMetadata = @"metadata";
static NSString *const HKPluginKeyValue = @"value";
static NSString *const HKPluginKeyCorrelationType = @"correlationType";
static NSString *const HKPluginKeySampleType = @"sampleType";
static NSString *const HKPluginKeyUnit = @"unit";
static NSString *const HKPluginKeyRecordId = @"recordId";
static NSString *const HKPluginKeyNotificationUrl = @"notificationUrl";


+ (HKHealthStore *)sharedHealthStore {
    __strong static HKHealthStore *store = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        store = [[HKHealthStore alloc] init];
    });
    
    return store;
}

-(void)sendNotification:(NSString*)notificationUrl data:(NSMutableDictionary*)data completionHandler:(void(^)(BOOL success))completionHandler{
    
    UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    
    //if([UIApplication sharedApplication].applicationState != UIApplicationStateActive){
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"MyTask" expirationHandler:^{
        // Clean up any unfinished task business by marking where you
        // stopped or ending the task outright.
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        //bgTask = UIBackgroundTaskInvalid;
    }];
    //}
    
    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        
        
        //Do the work associated with the task, preferably in chunks.
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:notificationUrl]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setTimeoutInterval:60];
        
        NSString *json = [self jsonFromDictionary:data];
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:jsonData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            
            if(httpResponse.statusCode == 200)
            {
                completionHandler(true);
            }
            else
            {
                completionHandler(false);
            }
        }];
        [dataTask resume];
        
    });
    
    
    if (bgTask != UIBackgroundTaskInvalid)
    {
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
    }
    
    
}

+ (NSString *)stringFromDate:(NSDate *)date {
    __strong static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    });
    
    return [formatter stringFromDate:date];
}

-(NSString*)jsonFromDictionary:(NSDictionary*)data{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = nil;
    if (! jsonData) {
        NSLog(@"Error converting data to JSON : %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

+ (HKSampleType *)getHKSampleType:(NSString *)elem {
    
    HKSampleType *type = nil;
    
    type = [HKObjectType quantityTypeForIdentifier:elem];
    if (type != nil) {
        return type;
    }
    
    type = [HKObjectType categoryTypeForIdentifier:elem];
    if (type != nil) {
        return type;
    }
    
    type = [HKObjectType correlationTypeForIdentifier:elem];
    if (type != nil) {
        return type;
    }
    
    if ([elem isEqualToString:@"workoutType"]) {
        return [HKObjectType workoutType];
    }
    
    if (@available(iOS 11.0, *)) {
        type = [HKObjectType seriesTypeForIdentifier:elem];
        if (type != nil) {
            return type;
        } else {
            // Fallback on earlier versions
        }
    }
    
    if (@available(iOS 12.0, *)) {
        type = [HKObjectType clinicalTypeForIdentifier:elem];
        if (type != nil) {
            return type;
        }
    }
    
    // leave this here for if/when apple adds other sample types
    return type;
    
}

-(void)querySampleType:(HKSampleType*)type sampleTypeString:(NSString*)sampleTypeString startDate:(NSDate*)startDate endDate:(NSDate*)endDate unit:(HKUnit*)unit limit:(NSUInteger)limit ascending:(BOOL)ascending completionHandler:(void(^)(NSMutableArray *results))completionHandler{
    NSString *endKey = HKSampleSortIdentifierEndDate;
    NSSortDescriptor *endDateSort = [NSSortDescriptor sortDescriptorWithKey:endKey ascending:ascending];
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type
                                                           predicate:predicate
                                                               limit:limit
                                                     sortDescriptors:@[endDateSort]
                                                      resultsHandler:^(HKSampleQuery *sampleQuery,
                                                                       NSArray *results,
                                                                       NSError *innerError) {
                                                          if (innerError != nil) {
                                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                                  //ignoring error for now
                                                              });
                                                          } else {
                                                              NSMutableArray *finalResults = [[NSMutableArray alloc] initWithCapacity:results.count];
                                                              
                                                              for (HKSample *sample in results) {
                                                                  
                                                                  NSMutableDictionary* entry = [self transformSample:sample sampleTypeString:sampleTypeString  unit:unit];
                                                                  
                                                                  [finalResults addObject:entry];
                                                              }
                                                              
                                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                                  completionHandler(finalResults);
                                                              });
                                                          }
                                                      }];
    
    [[HKitMonitor sharedHealthStore] executeQuery:query];
}

-(NSMutableDictionary*)transformSample:(HKSample*)sample sampleTypeString:(NSString*) sampleTypeString unit:(HKUnit*)unit{
    NSDate *startSample = sample.startDate;
    NSDate *endSample = sample.endDate;
    NSMutableDictionary *entry = [NSMutableDictionary dictionary];
    
    // common indices
    entry[HKPluginKeyStartDate] = [NSString stringWithFormat:@"%f", [startSample timeIntervalSince1970]];
    entry[HKPluginKeyEndDate] = [NSString stringWithFormat:@"%f", [endSample timeIntervalSince1970]];
    entry[@"uuid"] = sample.UUID.UUIDString;
    
    entry[@"timeZoneSecondsFromGMT"] = [[NSNumber numberWithDouble:[[NSTimeZone localTimeZone] secondsFromGMTForDate:startSample]] stringValue];
    
    entry[@"itemType"] = sampleTypeString;
    
    //@TODO Update deprecated API calls
    entry[HKPluginKeySourceName] = sample.source.name;
    entry[HKPluginKeySourceBundleId] = sample.source.bundleIdentifier;
    
    entry[HKPluginKeyUnit] = [unit unitString];
    
    
    if (sample.metadata == nil || ![NSJSONSerialization isValidJSONObject:sample.metadata]) {
        entry[HKPluginKeyMetadata] = @{};
    } else {
        entry[HKPluginKeyMetadata] = sample.metadata;
    }
    
    // case-specific indices
    if ([sample isKindOfClass:[HKCategorySample class]]) {
        
        HKCategorySample *csample = (HKCategorySample *) sample;
        entry[HKPluginKeyValue] = @(csample.value);
        entry[@"categoryType.identifier"] = csample.categoryType.identifier;
        entry[@"categoryType.description"] = csample.categoryType.description;
        
    } else if ([sample isKindOfClass:[HKCorrelationType class]]) {
        
        HKCorrelation *correlation = (HKCorrelation *) sample;
        entry[HKPluginKeyCorrelationType] = correlation.correlationType.identifier;
        
    } else if ([sample isKindOfClass:[HKQuantitySample class]]) {
        
        HKQuantitySample *qsample = (HKQuantitySample *) sample;
        [entry setValue:@([qsample.quantity doubleValueForUnit:unit]) forKey:@"quantity"];
        [entry setValue:@([qsample.quantity doubleValueForUnit:unit]) forKey:@"value"];
        
    } else if ([sample isKindOfClass:[HKWorkout class]]) {
        
        HKWorkout *wsample = (HKWorkout *) sample;
        [entry setValue:@(wsample.duration) forKey:@"duration"];
        
    }
    return entry;
}

-(void)saveAnchors:(NSMutableDictionary *)anchors{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:anchors];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"HKitMonitor_Anchors"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSMutableDictionary *)getAnchors{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"HKitMonitor_Anchors"];
    if(!data){
        return [[NSMutableDictionary alloc] init];
    }
    NSMutableDictionary *anchors = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return anchors;
}

+ (HKObjectType *)getHKObjectType:(NSString *)elem {
    
    HKObjectType *type = nil;
    
    type = [HKObjectType quantityTypeForIdentifier:elem];
    if (type != nil) {
        return type;
    }
    
    type = [HKObjectType characteristicTypeForIdentifier:elem];
    if (type != nil) {
        return type;
    }
    
    if (@available(iOS 12.0, *)) {
        type = [HKObjectType clinicalTypeForIdentifier:elem];
        if (type != nil) {
            return type;
        }
    }
    
    // @TODO | The fall through here is inefficient.
    // @TODO | It needs to be refactored so the same HK method isnt called twice
    return [HKitMonitor getHKSampleType:elem];
}

-(void)finishedLaunching:(NSNotification*)notification{
    
    NSString *recordId = [SAMKeychain passwordForService:@"PersonCredentials" account:@"PersonCredentials_HKRecordId"];
    
    NSString *personId = [SAMKeychain passwordForService:@"PersonCredentials" account:@"PersonCredentials_HKPersonId"];
    
    NSString *notificationUrl = [SAMKeychain passwordForService:@"PersonCredentials" account:@"PersonCredentials_HKNotificationUrl"];
    
    if(recordId != nil && notificationUrl != nil && personId != nil)
    {
        NSMutableSet *readDataTypes = [[NSMutableSet alloc] init];
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"mg/dL", @"HKQuantityTypeIdentifierBloodGlucose",
                              @"count/min", @"HKQuantityTypeIdentifierHeartRate",
                              @"m", @"HKQuantityTypeIdentifierDistanceCycling",
                              @"m", @"HKQuantityTypeIdentifierDistanceWalkingRunning",
                              @"%", @"HKQuantityTypeIdentifierOxygenSaturation",
                              @"mmHg", @"HKQuantityTypeIdentifierBloodPressureSystolic",
                              @"mmHg", @"HKQuantityTypeIdentifierBloodPressureDiastolic",
                              nil];
        HKObjectType *type = nil;
        for (NSString *typeString in dict) {
            
            if ([typeString isEqual:@"HKWorkoutTypeIdentifier"]) {
                type = [HKObjectType workoutType];
            } else {
                type = [HKitMonitor getHKObjectType:typeString];
            }
            if(type){
                [readDataTypes addObject:type];
            }
        }
        if(readDataTypes.count > 0){
            [[HKitMonitor sharedHealthStore]  requestAuthorizationToShareTypes:nil readTypes:readDataTypes completion:^(BOOL success, NSError *error) {
                if (!success) {
                    NSLog(@"You didn't allow HealthKit to access these read/write data types. The error was: %@.", error);
                    return;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSString *typeString in dict) {
                        NSString *unit = [dict objectForKey:typeString];
                        
                        [self monitorSampleType:typeString unitString:unit recordId:recordId personId:personId notificationUrl:notificationUrl];
                    }
                });
            }];
        }
        
        
        
    }
}

@end


@implementation HKitMonitor

-(void)pluginInitialize{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishedLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
}



- (void)monitorSampleType:(CDVInvokedUrlCommand *)command {
    NSDictionary *args = command.arguments[0];
    NSString *sampleTypeString = args[HKPluginKeySampleType];
    NSString *unitString = args[HKPluginKeyUnit];
    NSString *recordId = args[HKPluginKeyRecordId];
    NSString *personId = args[@"personId"];
    NSString *notificationUrl = args[HKPluginKeyNotificationUrl];
    [self monitorSampleType:sampleTypeString unitString:unitString recordId:recordId personId: personId notificationUrl:notificationUrl];
}

-(void)monitorSampleType:(NSString *)sampleTypeString unitString:(NSString *)unitString recordId:(NSString *) recordId personId: (NSString *) personId notificationUrl: (NSString *)notificationUrl {
    HKSampleType *type = [HKitMonitor getHKSampleType:sampleTypeString];
    HKUpdateFrequency updateFrequency = HKUpdateFrequencyImmediate;
    if (type == nil) {
        return;
    }
    
    HKUnit *unit = nil;
    if (unitString != nil) {
        // issue 51
        // @see https://github.com/Telerik-Verified-Plugins/HealthKit/issues/51
        if ([unitString isEqualToString:@"percent"]) {
            unitString = @"%";
        }
        unit = [HKUnit unitFromString:unitString];
    }
    
    // TODO use this an an anchor for an achored query
    //__block int *anchor = 0;
#ifdef HKPLUGIN_DEBUG
    NSLog(@"Setting up ObserverQuery");
#endif
    
    HKObserverQuery *query;
    query = [[HKObserverQuery alloc] initWithSampleType:type
                                              predicate:nil
                                          updateHandler:^(HKObserverQuery *observerQuery,
                                                          HKObserverQueryCompletionHandler handler,
                                                          NSError *error) {
                                              
                                              
                                              if (error) {
                                                  
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                      NSLog(@"Observer query stopped with error : %@", error);
                                                  });
                                                  
                                              } else {
                                                  
#ifdef HKPLUGIN_DEBUG
                                                  NSLog(@"HealthKit plugin received a monitorSampleType, passing it to JS.");
#endif
                                                  NSMutableDictionary *anchors = [self getAnchors];
                                                  HKQueryAnchor *anchor = nil;
                                                  if(![[anchors allKeys] containsObject:type.identifier]){
                                                      anchors[type.identifier] = [HKQueryAnchor anchorFromValue:HKAnchoredObjectQueryNoAnchor];
                                                      [self saveAnchors:anchors];
                                                  }
                                                  anchor = [anchors objectForKey: type.identifier];
                                                  
                                                  
                                                  HKAnchoredObjectQuery *anchorQuery = [[HKAnchoredObjectQuery alloc] initWithType:type predicate:nil anchor:anchor limit:HKObjectQueryNoLimit resultsHandler:^(HKAnchoredObjectQuery * query,
                                                                                                                                                                                                                NSArray<HKSample *> * results,
                                                                                                                                                                                                                NSArray<HKDeletedObject *> * deletedObjects,
                                                                                                                                                                                                                HKQueryAnchor *newAnchor,
                                                                                                                                                                                                                NSError * error){
                                                      
                                                      
                                                      anchors[type.identifier] = newAnchor;
                                                      [self saveAnchors:anchors];
                                                      
                                                      NSMutableArray *itemArray = [[NSMutableArray alloc] init];
                                                      
                                                      for (HKSample *sample in results) {
                                                          
                                                          NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                                                          NSMutableDictionary *itemData = [self transformSample:sample sampleTypeString:type.identifier unit:unit ];
                                                          
                                                          [item setValue: itemData forKey:@"itemData"];
                                                          [item setValue: recordId forKey:@"recordId"];
                                                          [item setValue: type.identifier forKey:@"itemType"];
                                                          [item setValue: [NSNumber numberWithInt:0] forKey:@"isDeleted"];
                                                          [item setValue: recordId forKey:@"recordId"];
                                                          [itemArray addObject:item];
                                                      }
                                                      
                                                      for (HKDeletedObject * sample in deletedObjects) {
                                                          NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                                                          NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
                                                          itemData[@"uuid"] = sample.UUID.UUIDString;
                                                          item[@"recordId"] = recordId;
                                                          item[@"itemType"] = type.identifier;
                                                          [item setValue: [NSNumber numberWithInt:1] forKey:@"isDeleted"];
                                                          item[@"itemData"] = itemData;
                                                          [itemArray addObject:item];
                                                      }
                                                      
                                                      if(itemArray.count > 0){
                                                          NSMutableDictionary *finalData = [[NSMutableDictionary alloc] init];
                                                          [finalData setValue:itemArray forKey:@"data"];
                                                          [finalData setValue:personId forKey:@"personId"];
                                                          NSLog(@"Result obtained from querySampleType after notification %@", finalData);
                                                          [self sendNotification:notificationUrl data:finalData completionHandler:^(BOOL success) {
                                                              if(success){
                                                                  //                                                                  anchors[sampleTypeString] = newAnchor;
                                                                  //                                                                  [self saveAnchors:anchors];
                                                              }
                                                              anchors[type.identifier] = newAnchor;
                                                              [self saveAnchors:anchors];
                                                              handler();
                                                          }];
                                                      }
                                                      else{
                                                          handler();
                                                      }
                                                      
                                                  }];
                                                  
                                                  [[HKitMonitor sharedHealthStore] executeQuery:anchorQuery];
                                                  
                                              }
                                              
                                          }];
    
    
    // Make sure we get the updated immediately
    [[HKitMonitor sharedHealthStore] enableBackgroundDeliveryForType:type frequency:updateFrequency withCompletion:^(BOOL success, NSError *error) {
        
        if (success) {
            NSLog(@"Background devliery enabled %@", sampleTypeString);
        } else {
            NSLog(@"Background delivery not enabled for %@ because of %@", type.identifier, error);
        }
        NSLog(@"Executing ObserverQuery");
        
        [[HKitMonitor sharedHealthStore] executeQuery:query];
        // TODO provide some kind of callback to stop monitoring this value, store the query in some kind of WeakHashSet equilavent?
    }];
}


@end
